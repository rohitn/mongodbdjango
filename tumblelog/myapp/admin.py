from django.contrib import admin
from models import Link, Folder

class LinkInline(admin.StackedInline):
  model = Folder.links.through
  
class LinkAdmin(admin.ModelAdmin):
    pass
admin.site.register(Link)

class FolderAdmin(admin.ModelAdmin):
  inlines = [LinkInline,]
admin.site.register(Folder, FolderAdmin)