from django.db import models
import datetime

class Link(models.Model):
    url = models.URLField()
    description = models.TextField(null=True, blank=True)
    date_submitted = models.DateTimeField(default=datetime.datetime.now)
    name = models.CharField(max_length=200)
    
    def __unicode__(self):
        return self.name

class Folder(models.Model):
    name = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)
    links = models.ManyToManyField(Link)
    
    def __unicode__(self):
        return self.name
