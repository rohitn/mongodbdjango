from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.views.generic import DetailView
from tumblelog.forms import CommentForm
from django.shortcuts import get_object_or_404, render_to_response
from tumblelog.models import Post, Comment
from django.template import RequestContext
from django.utils import simplejson


class PostDetailView(DetailView):
    methods = ['get', 'post']

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = CommentForm(object=self.object)
        context = self.get_context_data(object=self.object, form=form)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = CommentForm(object=self.object, data=request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(self.object.get_absolute_url())

        context = self.get_context_data(object=self.object, form=form)
        return self.render_to_response(context)

def ShowAllPosts(request, template_name="show_posts.html"):
    posts = Post.objects.all()
    return render_to_response(template_name, locals(),
                              context_instance=RequestContext(request))

def ShowPost(request, slug, template_name="show_post.html"):
    post = Post.objects.get(slug=slug)
    return render_to_response(template_name, locals(),
                              context_instance=RequestContext(request))

def UpdatePostComments(request):
    postdata = request.POST.copy()
    post_id = postdata["post-id"]
    comment_id = postdata["comment-hash"]
    comment_author = postdata["comment-author"]
    comment_body = postdata["comment-body"]
    comment_enabled = int(postdata["comment-status"])    
    post = Post.objects.get(pk=post_id)
    reloaded_comments = []
    for comment in post.comments:
        if int(comment.hash) == int(comment_id):
            if comment_enabled:
                new_comment = Comment(body=comment_body, author=comment_author)
                reloaded_comments.append(new_comment)
        else:
            reloaded_comments.append(comment)
    post.comments = reloaded_comments
    post.save()
    return HttpResponseRedirect("/showpost/%s" % post.slug)

def UpdatePostAttributes(request):
    postdata = request.POST.copy()
    post_id = postdata["post-id"]
    extension = int(postdata["extension"])
    post = Post.objects.get(pk=post_id)
    if extension:
        new_dict = {}
        for key, val in postdata.iteritems():
            if "key" in key:
                counter = key.split("-")
                if len(counter) == 2:
                    counter = counter[1]
                    value_id = "value-" + counter
                    if value_id in postdata.keys():
                        attribute_value = postdata[value_id]
                        new_dict[val] = attribute_value
                    else:
                        print "no value_id in postdata"
                else:
                    print "counter lenght not 2"
        post.attributes = new_dict
        post.save()
    else:
        key = postdata['key-0']
        value = postdata['value-0']
        post.attributes = {key: value}
        post.save()
    return HttpResponseRedirect("/showpost/%s" % post.slug)
    

    