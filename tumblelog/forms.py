from django.forms import ModelForm
from tumblelog.models import Post, Comment
import time


class CommentForm(ModelForm):

    def __init__(self, object, *args, **kwargs):
        """Override the default to store the original document
        that comments are embedded in.
        """
        self.object = object
        return super(CommentForm, self).__init__(*args, **kwargs)

    def save(self, *args):
        """Append to the comments list and save the post"""
        self.instance.hash = int(time.time()*1e6)
        self.object.comments.append(self.instance)
        self.object.save()
        return self.object

    class Meta:
        model = Comment
        exclude = ('status',)

class PostForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PostForm,self).__init__(*args, **kwargs)
        self.fields['comments'].widget.choices = (
          [(i.pk, i) for i in self.instance.comments])
        if self.instance.pk:
            self.fields['comments'].initial = self.instance.comments

    class Meta:
        model = Post
