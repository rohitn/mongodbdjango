from django.contrib import admin
from tumblelog.models import Post
from tumblelog.forms import PostForm

class PostAdmin(admin.ModelAdmin):
    form = PostForm

    def __init__(self, model, admin_site):
        super(PostAdmin,self).__init__(model, admin_site)

admin.site.register(Post, PostAdmin)
