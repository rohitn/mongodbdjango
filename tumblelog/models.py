from django.db import models
from django.core.urlresolvers import reverse
from django import forms
from djangotoolbox.fields import ListField, EmbeddedModelField, DictField
from django_mongodb_engine.contrib import MongoDBManager
import time
import datetime

POST_CHOICES = (
    ('p', 'post'),
    ('v', 'video'),
    ('i', 'image'),
    ('q', 'quote'),
)

class ModelListField(ListField):
    def formfield(self, **kwargs):
        return FormListField(**kwargs)

class ListFieldWidget(forms.SelectMultiple):
    pass

class FormListField(forms.MultipleChoiceField):
    """
        This is a custom form field that can display a ModelListField as a Multiple Select GUI element.
    """
    widget = ListFieldWidget

    def clean(self, value):
        return value


class Post(models.Model):
    objects = MongoDBManager()
    created_at = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=255)
    slug = models.SlugField()
    comments = ModelListField(EmbeddedModelField('Comment'), editable=True)
    post_type = models.CharField(max_length=1, choices=POST_CHOICES, default='p')
    body = models.TextField(blank=True, help_text="The body of the Post / Quote")
    embed_code = models.TextField(blank=True, help_text="The embed code for video")
    image_url = models.URLField(blank=True, help_text="Image src")
    author = models.CharField(blank=True, max_length=255, help_text="Author name")
    attributes = DictField(editable=False)

    def get_absolute_url(self):
        return reverse('post', kwargs={"slug": self.slug})

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ["-created_at"]


class Comment(models.Model):
    objects = MongoDBManager()
    hash = models.AutoField(default=int(time.time()*1e6), primary_key=True, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    body = models.TextField(verbose_name="Comment")
    author = models.CharField(verbose_name="Name", max_length=255)
    status = models.BooleanField(default=True)
    
    def __unicode__(self):
        return self.author